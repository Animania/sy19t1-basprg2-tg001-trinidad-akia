#include <iostream>
#include <time.h>
#include <conio.h>

using namespace std;

struct item 
{
	string Name;
	int Gold;
}lootedItem;

//Exercise 3-4
item GenerateItems()
{
	item loot;
	string itemnames[5] = { "HQM Ore", "Sulfure Ore", "Metal Ore", "Stone Ore", "Cursed Stone" };
	int itemvalues[5] = { 100, 50, 25, 5, 0 };
	int randomize;

	//Generates a random number from 1 to 5 to identify the item loot
	randomize = rand() % 5;
	loot.Name = itemnames[randomize];
	loot.Gold = itemvalues[randomize];

	return loot;
}


//Exercise 3-5
void DungeonEntrance(int& Gold)
{
	char cont;
	int TotalLootedgold = 0, multiplier = 1;

	//gold reduces 25 when player enters dungeon
	Gold = Gold - 25;

	do
	{
		system("cls");

		//call out function to genarate item to loot
		lootedItem = GenerateItems();

		//checks if its a cursed stone
		if (lootedItem.Name == "Cursed Stone")
		{
			cout << "You looted a Cursed stone! You will lose all your looted gold and will exit the dungeon now." << endl;
			system("pause");
			//sets the total gold to 0 and breaks
			TotalLootedgold = 0; break;
		}
		//if not cursed stone, code below runs
		//Shows the name of the Item looted and its price * Multiplier
		cout << "You looted  " << lootedItem.Name
			<< " Worth " << lootedItem.Gold << " (x" << multiplier << ')' << endl;

		//computes the total looted gold
		TotalLootedgold = TotalLootedgold + (lootedItem.Gold * multiplier);
		cout << "Total looted gold:\t" << TotalLootedgold << endl;

		//will ask if the player still wants to continue
		cout << "\nPress any buttn to continue looting or press X to exit the dungeon";
		cont = _getch();

		multiplier++;
		if (multiplier >= 4) { multiplier = 4; }

	} while (cont != 'x');

	//when the player exits the dungeon the looted gold adds to the current players gold
	Gold = Gold + TotalLootedgold;
}

//Exercise 3-6
int main() 
{
	srand(time(NULL));
	int PGOLD = 50;

	//Checks if the player still has gold to pay or if the player has more than 500 gold
	while ((PGOLD >= 25) && (PGOLD < 500)) 
	{
		system("cls");

		cout << "----------Mini Game Dungeon: Loot----------"
			<< "\n\n-You will need to pay 25 gold to enter the dungeon"
			<< "\n-Special items are in the dungeon!"
			<< "\n-When exiting the dungeon, all looted items will be added to your current gold"
			<< "\n\nPress any button to Enter Dungeon. Your Total Gold is:\t" << PGOLD; _getch();
		DungeonEntrance(PGOLD);
	};

	system("cls");

	//Exercise 3-6
	if (PGOLD >= 500) 
	{
		cout << "Reached Gold Win Limit! You have " << PGOLD << " Gold. Thank you for playing!" << endl;
	}
	else if (PGOLD <= 0) { cout << "Insufficient Gold!\n" << endl; }
	else { cout << "You do not meet the required gold to enter the Dungeon.\n" << endl; }

	system("pause");
	return 0;
}
