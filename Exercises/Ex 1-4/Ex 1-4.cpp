#include <iostream>
#include <string>

using namespace std;

void sortArray(int array[])
{
	int small;
	for (int i = 0; i < 10; i++)
	{
		for (int x = i + 1; x < 10; x++)
		{
			if (array[x] < array[i])
			{
				small = array[i];
				array[i] = array[x];
				array[x] = small;
			}
		}
		cout << array[i] << ", ";

	}



}

int main()
{
	int number[10], inputNum, largest = 0;

	cout << "Enter 10 random numbers:\n";
	for (int j = 0; j < 10; j++)
	{
		cout << j + 1 << ", ";
		cin >> inputNum;
		number[j] = inputNum;
		if (largest < inputNum)
		{
			largest = inputNum;
		}
	}

	system("cls");


	cout << "User generated array:\t";
	for (int a = 0; a < 10; a++)
	{
		cout << number[a] << ", ";
	}
	cout << endl;

	cout << "Sorted array:\t\t";
	sortArray(number);
	cout << "\nThe Largest number is " << largest << endl;

	system("pause");
	return 0;
}