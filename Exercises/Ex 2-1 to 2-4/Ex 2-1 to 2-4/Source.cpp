#include <iostream>
#include <time.h>
#include <conio.h>

using namespace std;

struct roll
{
	int dice[2];
	int value;
}player, dealer;

//Exercise 2-1
int bet1(int& gold)
{
	int bet;

	//The player can only only bet more than 0 or less then the remaining gold the player has
	do {
		cout << "Place Bet (Gold: " << gold << "): ";
		cin >> bet;
	} while ((bet != 0) && (bet > gold));

	//The bet placed will reduce the players gold
	gold = gold - bet;

	return bet;
}
void bet2(int& gold, int& bet)
{
	//The player can only only bet more than 0 or less then the remaining gold the player has
	do {
		cout << "Place Bet (Gold: " << gold << "): ";
		cin >> bet;
	} while ((bet == 0) && (bet > gold));

	//The bet placed will reduce the players gold
	gold = gold - bet;
}

//Exercise 2-2
void rollDice(roll& roller)
{
	for (int i = 0; i < 2; i++)
	{
		roller.dice[i] = rand() % 6 + 1;

		cout << "Dice " << i + 1 << ": " << roller.dice[i] << endl;
	}
	roller.value = roller.dice[0] + roller.dice[1];
}

//Exercise 2-3
void payout(roll player, roll dealer, int bet, int& gold)
{
	int pay = 0;

	//If the player and the dealer has rolled the same dice, bet will return
	if (player.value == dealer.value)
	{
		pay = 0; cout << "\nDraw!" << endl;
	}

	//If the player has rolled a snake eye, he wins3 times the bet
	else if (player.value == 2)
	{
		pay = bet * 3; cout << "\nPlayers bet multiplies by 3" << endl;
	}

	//If the dealer rolled a higher dice, the player will lose the bet
	else if ((player.value < dealer.value) || (dealer.value == 2))
	{
		pay = -bet; cout << "\nPlayer has lost" << endl;
	}

	//If the player rolled a higher Dice, the player then wins back the bet added the amount of bet
	else if (player.value > dealer.value)
	{
		pay = bet; cout << "\nPlayer has Won!" << endl;
	}

	cout << "Payout:\t" << pay << endl;
	//The payout will be added in the Gold
	gold = gold + bet + pay;
	cout << "Gold:\t" << gold << endl;
}

//EXERCISE 2-4
void playRound(int& gold)
{
	int bet;

	system("cls");
	//Player will place a bet 
	bet = bet1(gold);

	system("cls");

	//Players turn to roll dice
	cout << "PLAYER DICE" << endl;
	rollDice(player);

	//Dealers turn to roll the dice
	cout << "\nDEALER DICE" << endl;
	rollDice(dealer);

	//calculates the payout if its a win or lose
	payout(player, dealer, bet, gold);
}

int main()
{
	srand(time(NULL));
	int gold = 1000;
	char playAgain;

	//The game ends if the player has no more gold or wants to quit
	do
	{
		system("cls");
		playRound(gold);
		cout << "Press any button to play again or press \"x\" to quit. " << endl;
		playAgain = _getch();
	} while ((gold > 0) && (playAgain != 'x'));

	system("cls");
	if (gold <= 0)
	{
		cout << "Insufficient Gold! You lose!" << endl;
	}
	cout << "Thank you for participating!" << endl;

	system("pause");
	return 0;
}