#pragma once
#include "Move.h"
#include "Equipment.h"
using namespace std;

class Character
{
private:
	string name;
	string status;
	//string materia;

	int lv;
	int hp;
	int mp;
	int exp;
	int nextlv;
	int limitlv;
	int strength;
	int dexterity;
	int vitality;
	int magic;
	int spirit;
	int luck;
	int attack;
	int attackPercent;
	int defense;
	int defensePercent;
	int magicatk;
	int magicdef;
	int magicdefPercent;
	int maxHp;
	int maxMp;

	Equipment* currentEquiped;

public:
	Character();
	~Character();

	void inputCharacterStats();

	string getName();
	void setMp(int value);
	int getMp();

	void setHP(int value);
	int getHP();

	void setLevel(int value);
	void setExp(int value);
	int getExp();

	void equipEquipment(Equipment* eq);
	void modifyEquipment();

};#pragma once
