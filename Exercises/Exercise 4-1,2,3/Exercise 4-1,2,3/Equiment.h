#pragma once
#include <string>
using namespace std;

class Equipment
{
private:

	string wpn;
	string arm;
	string acc;
	string item;

	int wpnStats;
	int armStats;
	int accStats;
	int itemStats;

public:
	Equipment();
	~Equipment();

	void equipMateria();
};

