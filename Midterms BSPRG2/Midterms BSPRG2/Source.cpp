#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

const int minimumMM = 1;
const int maximumMM = 30;

struct cardecks
{
	vector<string> cards;
	string cardpick;

}player, enemy;

//Creates or generates the cards on the hands
cardecks generateCards(string side)
{
	cardecks hand;
	int randPosition = rand() % 5;



	//makes the emperor cards to be generated

	vector<string> cards;
	for (int i = 0; i < 5; i++)
	{
		if (i == randPosition) { cards.push_back(side); }
		else { cards.push_back("civilian"); }
	}

	hand.cards = cards;

	return hand;
}

//will prompt the player/kaiji to bet
int milimeterBet() 
{
	int milimiter;

	//it will loop if the player/kaiji bets below 1mm or if it is above 30mm
	do 
	{
		cout << "How many millimeters will you bet?"
			<< "\n(You can only bet from 1mm - 30mm): ";
		cin >> milimiter;
	} while (milimiter < minimumMM && milimiter > maximumMM);

	return milimiter;
}

//player(kaiji) will have its decks changed after three rounds and this will enumerate the multiplier
int pickDeck(cardecks& player, cardecks& enemy, int round) 
{
	//thill will create the emperor cards for the player from the rounds 1-3 and also 7-9
	if (round <= 3 || (round >= 7 && round <= 9))
	{
		cout << "Players Deck: Emperor!"
			<< "\nOpponents Deck: Slave!" << endl;
		player = generateCards("emperor");
		enemy = generateCards("slave");
		return 100000;
	}
 
	//this will create the salvee cards for the player from the rounds 4-6 and also for 10-12
	else
	{
		cout << "Players Deck: Slave!"
			<< "\nOpponents DECK: Emperor!" << endl;
		player = generateCards("slave");
		enemy = generateCards("emperor");
		return 500000;
	}
}

bool comparePick(string player, string enemy)
{
	//if both the player and opponent gets ciivilian
	if (player == enemy)
	{
		cout << "Draw!" << endl;
		return false;
	}
	else if (player == "emperor") 
	{
		if (enemy == "slave") 
		{ return false; }
		else { return true; }
	}
	else if (player == "slave")
	{
		if (enemy == "civilian") 
		{ return false; }
		else { return true; }
	}
	else if (player == "civilian")
	{
		if (enemy == "emperor") 
		{ return false; }
		else { return true; }
	}
}

void playRound(int round, int& money, int& distance) 
{
	int milimiter, multiplier;
	string pick;
	bool win = true;

	milimiter = milimeterBet();
	system("cls");

	cout << "-ROUND " << round << "-" << endl;
	cout << "Player has bet " << milimiter << "Milimiter on this round." << endl;

	//this will show what the player and opponent have in their cards every round
	multiplier = pickDeck(player, enemy, round);

	cout << "\nPLACE YOUR CARDS!\n" << endl;
	while ((pick != "emperor" && pick != "slave") &&
		(enemy.cardpick != "emperor" && enemy.cardpick != "slave"))
	{

		//this will show the players deck on hand
		cout << "Cards on Hand: ";
		for (size_t i = 0; i < player.cards.size(); i++) 
		{
			cout << player.cards[i] << " ";
		}
		cout << endl;
		//player will play a card
		cout << "Player Will Play: ";
		cin >> pick;

		player.cardpick = pick;

		//eliminates the card at hand that is selected
		for (size_t k = 0; k < player.cards.size(); k++) 
		{
			if (pick == player.cards[k])
			{
				player.cards.erase(player.cards.begin() + k);
				break;
			}
		}

		//Opponent will pick a card
		cout << "Opponents turn to pick a card" << endl;
		enemy.cardpick = enemy.cards[(rand() % enemy.cards.size())];

		//eliminates the card on hand of the opponent. card that has been selected
		for (size_t e = 0; e < enemy.cards.size(); e++)
		{
			if (enemy.cardpick == enemy.cards[e])
			{
				enemy.cards.erase(enemy.cards.begin() + e);
				break;
			}
		}

		system("pause");
		system("cls");

		cout << "Players Card:\t" << player.cardpick
			<< "\nOpponents Card:\t" << enemy.cardpick << endl;

		//compares both the players and opponents card that has been selected
		win = comparePick(player.cardpick, enemy.cardpick);

		system("pause");
		system("cls");
	}

	//player win results to winning the payout and if the player lose, the mm will be moved
	if (win)
	{
		money = money + (milimiter * multiplier);
		cout << "You won this round Player! Your prize is " << milimiter * multiplier << " pericas. \nYou now have " << money << " pericas." << endl;
	}
	else
	{
		distance = distance + milimiter;
		cout << "You lose! The Object moves" << milimiter << "Milimiter closer.\nYou now have " << 30 - distance
			<< "Milimiter left before your ear becomes a blender." << endl;
	}
	system("pause");
	system("cls");
}

void ending(int money, int distance)
{
	if (distance >= 30)
	{
		cout << "You have lose the bet. Your ear...." << endl;
	}
	else if (money >= 20000)
	{
		cout << "Congratulations! You made it back in one piece and won " << money << "! What a nice way to end" << endl;
	}
	else if (money < 20000)
	{
		cout << "You only have " << money << " Perica but still you had your eardrums. So anticlimatic..." << endl;
	}
}

int main() 
{
	srand(time(NULL));
	int round = 1, money = 0, distance = 0;


	while (round <= 12 && distance < 30) 
	{
		playRound(round, money, distance);
		round++;
	};
	ending(money, distance);

	system("pause");
	return 0;
}